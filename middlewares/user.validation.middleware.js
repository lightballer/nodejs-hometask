const { user } = require('../models/user');
const UserService = require('../services/userService');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const keys = Object.keys(req.body);
    const fields = Object.keys(user);
    fields.shift();
    const { firstName, lastName, email, phoneNumber, password } = req.body;
    if (isKeysValid(keys, fields)) {
        if (!firstName) {
            throw new Error('Name not specified');
        } 
        if (!lastName) {
            throw new Error('Last name not specified');
        } 
        if (!isEmailValid(email)) {
            throw new Error('Email should be in format <emailaddress>@gmail.com');
        } 
        if (!isEmailUnique(email)) {
            throw new Error('Email already registered');
        }
        if (!isPhoneNumberValid(phoneNumber)) {
            throw new Error('Phone number should be in format +380xxxxxxxxx');
        }
        if (!isPhoneNumberUnique(phoneNumber)) {
            throw new Error('Phone number already registered');
        }
        if (!isPasswordValid(password)) {
            throw new Error('Minimum password length is 3');
        }
    }
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const { firstName, lastName, email, phoneNumber, password } = req.body;
    const keys = Object.keys(req.body);
    const fields = Object.keys(user);
    fields.shift();
    if (isKeysValid(keys, fields)) {
        if (email && !(isEmailValid(email))) {
            throw new Error('Email should be in format <emailaddress>@gmail.com');
        }
        if (email && !isEmailUnique(email)) {
            throw new Error('Email already registered');
        }
        if (phoneNumber && !isPhoneNumberValid(phoneNumber)) {
            throw new Error('Phone number should be in format +380xxxxxxxxx');
        }
        if (phoneNumber && !isPhoneNumberUnique(phoneNumber)) {
            throw new Error('Phone number already registered');
        }
        if (password && !isPasswordValid(password)) {
            throw new Error('Minimum password length is 3');
        }
    }
    next();
};

const isKeysValid = (keys, fields) => keys.length === keys.filter(key => fields.includes(key)).length;
const isEmailValid = email => email && email.match(/^[a-z0-9](\.?[a-z0-9]){5,}@gmail\.com$/);
const isPhoneNumberValid = phoneNumber => phoneNumber && phoneNumber.length === 13
    && +phoneNumber.substring(1)
    && phoneNumber.substring(0, 4) === '+380';
const isPasswordValid = password => password && password.length >= 3;
const isEmailUnique = email => !UserService.search({ email });
const isPhoneNumberUnique = phoneNumber => !UserService.search({ phoneNumber });

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
