const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const keys = Object.keys(req.body);
    const fields = Object.keys(fighter);
    fields.shift();
    const { name, power, defense, health = 100 } = req.body;
    if (isKeysValid(keys, fields)) {
        if (!name) {
            throw new Error('Name not specified');
        }
        if (!isNameUnique(name)) {
            throw new Error('Name already registered');
        }
        if (!isPowerValid(power)) {
            throw new Error('Invalid power value');
        }
        if (!isDefenseValid(defense)) {
            throw new Error('Invalid defense value');
        }
        if (!isHealthValid(health)) {
            throw new Error('Invalid health value');
        }
    }
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const keys = Object.keys(req.body);
    const fields = Object.keys(fighter);
    fields.shift();
    const { name, power, defense, health } = req.body;
    if (isKeysValid(keys, fields)) {
        if (name && !isNameUnique(name)) {
            throw new Error('Name already registered');
        }
        if (power && !isPowerValid(power)) {
            throw new Error('Invalid power value');
        }
        if (defense && !isDefenseValid(defense)) {
            throw new Error('Invalid defense value');
        } 
        if (health && !isHealthValid(health)) {
            throw new Error('Invalid health value');
        }
    }
    next();
}

const isKeysValid = (keys, fields) => keys.length === keys.filter(key => fields.includes(key)).length;
const isPowerValid = power => power && power > 1 && power < 100;
const isDefenseValid = defense => defense && defense > 1 && defense < 10;
const isHealthValid = health => health && health > 80 && health < 120;
const isNameUnique = name => !FighterService.search({ name });

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;