const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    if (res.err){
        const status = res.locals.status ? res.locals.status : 400;
        res.status(status).json({error: true, message: res.err.message});
    }
    if (res.data){
        res.status(200).json(res.data);
    }
}

exports.responseMiddleware = responseMiddleware;