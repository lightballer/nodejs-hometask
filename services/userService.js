const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    searchAll() {
        const items = UserRepository.getAll();
        if (!items) {
            return null;
        }
        return items;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    create(user) {
        const newUser = UserRepository.create(user);
        if (!newUser) {
            return null;
        } else {
            return newUser;
        }
    }

    update(id, user) {
        const updatedUser = UserRepository.update(id, user);
        if (!updatedUser) {
            return null;
        } else {
            return updatedUser;
        }
    }

    delete(id) {
        const deletedUser = UserRepository.delete(id); 
        return deletedUser;
    }
}

module.exports = new UserService();