const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    
    // TODO: Implement methods to work with fighters

    searchAll() {
        const items = FighterRepository.getAll();
        if (!items) {
            return null;
        }
        return items;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    create(fighter) {
        const newFighter = FighterRepository.create(fighter);
        if (!newFighter) {
            return null;
        } else {
            return newFighter;
        }
    }

    update(id, fighter) {
        const updatedFighter = FighterRepository.update(id, fighter);
        if (!updatedFighter) {
            return null;
        } else {
            return updatedFighter;
        }
    }

    delete(id) {
        const deletedFighter = FighterRepository.delete(id); 
        return deletedFighter;
    }
}

module.exports = new FighterService();