const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', (req, res, next) => {
    try {
        const fighters = FighterService.searchAll();
        if (!fighters) {
            res.locals.status = 404;
            throw new Error('No fighters found');
        }
        res.data = fighters;
    } catch (err) {
        res.err = err;
    } finally {
        next()
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const id = req.params.id;
        const fighter = FighterService.search({ id });
        if (!fighter) {
            res.locals.status = 404;
            throw new Error('Fighter not found');
        }
        res.data = fighter;
    } catch (err) {
        res.err = err;
    } finally {
        next()
    }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
    try {
        req.body.name = req.body.name.toLowerCase();
        const fighter = FighterService.create(req.body);
        if (!fighter) {
            throw new Error('Can`t create fighter');
        }
        res.data = fighter;
    } catch (err) {
        res.err = err;
    } finally {
        next()
    }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    try {
        req.body.name = req.body.name.toLowerCase();
        const id = req.params.id;
        const fighter = req.body;
        const updatedFighter = FighterService.update(id, fighter);
        if (!updatedFighter) {
            throw new Error('Can`t update fighter');
        }
        res.data = updatedFighter;
    } catch (err) {
        res.err = err;
    } finally {
        next()
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        const id = req.params.id;
        const deletedFighter = FighterService.delete(id);
        if (!deletedFighter) {
            throw new Error('Can`t delete fighter');
        }
        res.data = deletedFighter;
    } catch (err) {
        res.err = err;
    } finally {
        next()
    }
}, responseMiddleware);

module.exports = router;