const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', (req, res, next) => {
    try {
        const users = UserService.searchAll();
        if (!users) {
            res.locals.status = 404;
            throw new Error('No users found');
        }
        res.data = users;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const id = req.params.id;
        const user = UserService.search({ id });
        if (!user) {
            res.locals.status = 404;
            throw new Error('User not found');
        }
        res.data = user;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
    try {
        req.body.email = req.body.email.toLowerCase();
        const user = UserService.create(req.body);
        if (!user) {
            throw new Error('Can`t create user');
        }
        res.data = user;
    } catch (err) {
        res.err = err;
    } finally {
        next()
    }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    const id = req.params.id;
    req.body.email = req.body.email.toLowerCase();
    try {
        const user = req.body;
        const updatedUser = UserService.update(id, user);
        if (!updatedUser) {
            throw new Error('Can`t update user');
        }
        res.data = updatedUser;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    const id = req.params.id;
    try {
        const deletedUser = UserService.delete(id);
        if (!deletedUser) {
            throw new Error('Can`t delete user');
        }
        res.data = deletedUser;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;